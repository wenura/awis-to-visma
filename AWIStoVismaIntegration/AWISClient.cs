﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Configuration;
using System.Web;
using System.Web.Caching;
using System.Collections;
using System.IO;

namespace AWIStoVismaIntegration
{
    public class AWISClient
    {
        private static string baseAddress = string.Empty;
        HttpClient _httpClient;
        Common common;

        public AWISClient()
        {
            baseAddress = ConfigurationManager.AppSettings["awisAPIUrl"].ToString();
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri(baseAddress);
            common = new Common();
        }

        public bool Login(string userName, string password)
        {
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, "/AutoConcept.API/Token");

                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("grant_type", "password"));
                keyValues.Add(new KeyValuePair<string, string>("username", userName));
                keyValues.Add(new KeyValuePair<string, string>("password", password));
                keyValues.Add(new KeyValuePair<string, string>("client_id", "AutoConcept"));

                request.Content = new FormUrlEncodedContent(keyValues);
                var response = _httpClient.SendAsync(request);
                response.Wait();
                var contentStream = response.Result.Content.ReadAsStreamAsync();
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(LoginResult));

                LoginResult res = (LoginResult)serializer.ReadObject(contentStream.Result);
                if (HttpContext.Current != null)
                    HttpContext.Current.Cache.Remove("access_token");
                if (res.access_token != null)
                {
                    HttpRuntime.Cache.Add(
                        "access_token",
                        res.access_token,
                        null,
                        DateTime.Now.AddDays(7),
                        Cache.NoSlidingExpiration,
                        CacheItemPriority.Default,
                        null);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                common.AddErrorLog(ex);
                return false;
            }
            
        }

        public List<CountryViewModel> GetCountryList()
        {
            try
            {
                SetAuthorization();
                var request = new HttpRequestMessage(HttpMethod.Get, "/AutoConcept.API/api/AWIStoVisma/GetCountries");
                var response = _httpClient.SendAsync(request);
                response.Wait();
                var contentStream = response.Result.Content.ReadAsStreamAsync();
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<CountryViewModel>));
                List<CountryViewModel> res = (List<CountryViewModel>)serializer.ReadObject(contentStream.Result);

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GetCountryDBName(int countryId)
        {
            try
            {
                SetAuthorization();
                var request = new HttpRequestMessage(HttpMethod.Get, "/AutoConcept.API/api/AWIStoVisma/GetCountryDB?countryId=" + countryId.ToString());
                var response = _httpClient.SendAsync(request);
                response.Wait();
                var contentStream = response.Result.Content.ReadAsStreamAsync();
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(string));
                string countryDB = (string)serializer.ReadObject(contentStream.Result);
                Common.countryDBName = countryDB;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public InvoicesForVismaViewModel GetInvoicesList(string fromInvoiceNo, string toInvoiceNo, int countryId, DateTime toInvoiceDate)
        {
            try
            {
                InvoicesForVismaViewModel invoicesList = new InvoicesForVismaViewModel();
                SetAuthorization();
                var request = new HttpRequestMessage(HttpMethod.Get, "/AutoConcept.API/api/AWIStoVisma/GetInvoices?fromInvoiceNo=" + fromInvoiceNo + "&toInvoiceNo=" + toInvoiceNo + "&countryId=" + countryId + "&toInvoiceDate=" + toInvoiceDate.ToString("yyyyMMdd"));
                var response = _httpClient.SendAsync(request);
                response.Wait();
                var contentStream = response.Result.Content.ReadAsStreamAsync();
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(InvoicesForVismaViewModel));
                invoicesList = (InvoicesForVismaViewModel)serializer.ReadObject(contentStream.Result);
                return invoicesList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CustomerViewModel GetCustomerDetail(int customerId)
        {
            try
            {
                SetAuthorization();
                var request = new HttpRequestMessage(HttpMethod.Get, "/AutoConcept.API/api/AWIStoVisma/GetCustomerDetail?customerId=" + customerId);
                var response = _httpClient.SendAsync(request);
                response.Wait();
                var contentStream = response.Result.Content.ReadAsStreamAsync();
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(CustomerViewModel));
                CustomerViewModel customer = (CustomerViewModel)serializer.ReadObject(contentStream.Result);
                return customer;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #region Common
        public bool TestConnection()
        {
            try
            {
                //bool isOnline = false;
                SetAuthorization();
                var request = new HttpRequestMessage(HttpMethod.Get, "/AutoConcept.API/api/AWIStoVisma/TestConnection");
                var response = _httpClient.SendAsync(request);
                response.Wait();
                if (response.Result.StatusCode.ToString() == "OK")
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                common.AddErrorLog(ex);
                return false;
            }
        }

        public void SetAuthorization()
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", " " + (string)HttpRuntime.Cache["access_token"]);
        }

        public void AddErrorToAWIS(string loggedInUser, Exception exception)
        {
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, "/AutoConcept.API/api/AWIStoVisma/AddErrorLog");

                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("loggedinUser", loggedInUser));
                keyValues.Add(new KeyValuePair<string, string>("exception", exception.Message));
                keyValues.Add(new KeyValuePair<string, string>("innerException", exception.InnerException == null ? string.Empty : exception.InnerException.Message));
                keyValues.Add(new KeyValuePair<string, string>("application", "AwisToVISMA"));

                request.Content = new FormUrlEncodedContent(keyValues);
                var response = _httpClient.SendAsync(request);
                response.Wait();
                var contentStream = response.Result.Content.ReadAsStreamAsync();
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(int));
                serializer.ReadObject(contentStream.Result);
            }
            catch (Exception ex)
            {
                common.AddErrorLog(ex);
            }
        }
        #endregion
    }
}
