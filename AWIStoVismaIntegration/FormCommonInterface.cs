﻿using AWIStoVismaIntegration.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AWIStoVismaIntegration
{
    public partial class FormCommonInterface : Form
    {
        public FormCommonInterface()
        {
            InitializeComponent();
        }

        private void btnInvoice_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 frmInvoice = new Form2();
            frmInvoice.Show();
        }

        private void btnDirectPayments_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmDirectPayments frmDirectPayments = new frmDirectPayments();
            frmDirectPayments.Show();
        }

        private void FormCommonInterface_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to close this window?", Constant.appName, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                e.Cancel = false;
                Application.ExitThread();
            }
            else
            {
                e.Cancel = true;
            }
        }
    }
}