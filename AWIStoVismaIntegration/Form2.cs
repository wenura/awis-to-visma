﻿using AWIStoVismaIntegration.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AWIStoVismaIntegration
{
    public partial class Form2 : Form
    {
        AWISClient _awisClient;
        VISMAClient _vismaClient = new VISMAClient();
        Common _common;

        public Form2()
        {
            InitializeComponent();
            _awisClient = new AWISClient();
            _vismaClient = new VISMAClient();
            _common = new Common();
            try
            {
                comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
                List<CountryViewModel> countries = _awisClient.GetCountryList();
                comboBox1.ValueMember = "id";
                comboBox1.DisplayMember = "name";

                CountryViewModel testCountry = new CountryViewModel();
                testCountry.id = Constant.TESTBOLAG_ID;
                testCountry.name = "TESTBOLAG";
                countries.Add(testCountry);

                dateTimePicker1.Value = DateTime.Now;
                if (countries.Count > 0)
                {
                    _vismaClient.CloseDatabase();
                    comboBox1.DataSource = countries;
                    toolStripStatusLabel2.Text = "Online";
                    toolStripStatusLabel2.ForeColor = Color.Green;

                    toolStripStatusLabel3.Text = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                    timer1 = new Timer();
                    timer1.Tick += new EventHandler(timer1_Tick);
                    timer1.Interval = 10000;
                    timer1.Start();
                }
            }
            catch (Exception ex)
            {
                _awisClient.AddErrorToAWIS(Constant.userName, ex);
                _common.AddErrorLog(ex);

                MessageBox.Show(Constant.exceptionError, "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Hide();
                Form1 form1 = new Form1();
                form1.Show();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox1.Text = string.Empty;
            _vismaClient.CloseDatabase();
            Common.countryDBName = string.Empty;
            textBox2.Text = string.Empty;
            dateTimePicker1.Value = DateTime.Now;
            dataGridView1.Columns.Clear();
            textBox3.Text = string.Empty;
            textBox4.Text = string.Empty;
            textBox5.Text = string.Empty;
            toolStripProgressBar1.Value = 1;
            toolStripProgressBar1.Visible = false;
            button2.Enabled = true;
            ComboBox cmb = (ComboBox)sender;
            int selectedValue;
            if (int.TryParse(cmb.SelectedValue.ToString(), out selectedValue))
                GetLastInvoiceNo(selectedValue);
        }

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == 6 || e.ColumnIndex == 8)
            {
                e.CellStyle.Format = "N2";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string fromInvoiceNo = string.Empty;
                string toInvoiceNo = string.Empty;

                if (textBox2.Text == string.Empty)
                {
                    MessageBox.Show("Please enter AWIS last invoice no", Constant.appName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else if (Convert.ToInt32(textBox1.Text) >= Convert.ToInt32(textBox2.Text))
                {
                    MessageBox.Show("AWIS last invoice number cannot be less than VISMA invoice number", Constant.appName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else
                {
                    DateTime toInvoiceDate = dateTimePicker1.Value != null ? dateTimePicker1.Value : DateTime.Now;
                    fromInvoiceNo = textBox1.Text;
                    toInvoiceNo = textBox2.Text;
                    int selectedValue = (int)comboBox1.SelectedValue != 0 ? (int)comboBox1.SelectedValue : Constant.defaultCountry;

                    InvoicesForVismaViewModel invoices = new InvoicesForVismaViewModel();

                    invoices = _awisClient.GetInvoicesList(fromInvoiceNo, toInvoiceNo, selectedValue, toInvoiceDate);
                    int invoiceCount = invoices.Invoices.Count;
                    if (invoiceCount > 0)
                    {
                        dataGridView1.DataSource = invoices.Invoices;                        
                        
                        string grossValue = invoices.Invoices[0].GrossValue.ToString(); 
                        textBox3.Text = invoices.TotalGrossValue.ToString();
                        textBox4.Text = invoices.TotalTaxValue.ToString();
                        textBox5.Text = invoices.TotalNetValue.ToString();
                        button2.Enabled = true;
                    }
                    else
                    {
                        MessageBox.Show("Cannot find invoices!", Constant.appName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                _awisClient.AddErrorToAWIS(Constant.userName, ex);
                _common.AddErrorLog(ex);
                MessageBox.Show(Constant.exceptionError, "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            bool isOnline = _awisClient.TestConnection();
            if (isOnline)
            {
                toolStripStatusLabel2.Text = "Online";
                toolStripStatusLabel2.ForeColor = Color.Green;
            }
            else
            {
                toolStripStatusLabel2.Text = "Offline";
                toolStripStatusLabel2.ForeColor = Color.Red;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount > 0)
            {
                DialogResult result = MessageBox.Show("Are you sure?\n\nNote: This will generate Invoices, Invoice lines and relevent customers on VISMA", "Proceed!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    button2.Enabled = false;
                    ProceedInvoiceGeneration();
                }
            }
            else
                MessageBox.Show("Invoices List is empty", "Proceed!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ProceedInvoiceGeneration()
        {
            try
            {
                List<InvoicesViewModel> invoices = new List<InvoicesViewModel>();
                Result result = new Result();
                invoices = (List<InvoicesViewModel>)dataGridView1.DataSource;
                result = _vismaClient.ProceedInvoiceGeneration(invoices, Constant.userName, ref toolStripProgressBar1);
                if (result.isSuccess == true)
                {
                    MessageBox.Show("Invoice Process successfully completed!", Constant.appName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearScreen();
                }
                else
                {
                    MessageBox.Show(result.message, "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    button2.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GetLastInvoiceNo(int countryId)
        {
            try
            {
                int lastInvoiceNo = 0;
                if (countryId > 0)
                    _awisClient.GetCountryDBName(countryId);
                else
                    Common.countryDBName = Constant.testDB;

                int dbOpenResult;
                dbOpenResult = _vismaClient.DatabaseOpen();

                if (dbOpenResult == XORBase5.DbOk)
                {
                    lastInvoiceNo = _vismaClient.getRecentInvoiceNo();
                    _vismaClient.CloseDatabase();

                    if (lastInvoiceNo > 0)
                    {
                        textBox1.Text = lastInvoiceNo.ToString();
                    }
                    else
                    {
                        MessageBox.Show(Constant.invoiceNoNotFound, Constant.appName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    string error = _common.GetVISMAError(dbOpenResult, VismaMethods.DatabaseOpen);
                    MessageBox.Show(error, "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                _awisClient.AddErrorToAWIS(Constant.userName, ex);
                _common.AddErrorLog(ex);
                MessageBox.Show(Constant.exceptionError, "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClearScreen()
        {
            textBox1.Text = string.Empty;
            textBox2.Text = string.Empty;
            dateTimePicker1.Value = DateTime.Now;
            dataGridView1.Columns.Clear();
            textBox3.Text = string.Empty;
            textBox4.Text = string.Empty;
            textBox5.Text = string.Empty;
            toolStripProgressBar1.Value = 1;
            toolStripProgressBar1.Visible = false;
            button2.Enabled = true;
            GetLastInvoiceNo((int)comboBox1.SelectedValue);
        }

        private void Form2_Closing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to close this window?", Constant.appName, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                e.Cancel = false;
                Application.ExitThread();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 7)
            {
                dataGridView1.Rows[e.RowIndex].ErrorText = "";
                DateTime invoiceDate;
                if (dataGridView1.Rows[e.RowIndex].IsNewRow) { return; }
                if (!DateTime.TryParseExact(e.FormattedValue.ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out invoiceDate))
                {
                    e.Cancel = true;
                    dataGridView1.Rows[e.RowIndex].ErrorText = "Invoice date format must be YYYY-MM-DD";
                    button2.Enabled = false;
                }
                else
                {
                    DateTime dueDate;
                    DataGridViewRow dd = dataGridView1.Rows[e.RowIndex];
                    var ee = dd.Cells[9].Value;
                    dueDate = invoiceDate.AddDays(Convert.ToInt32(ee));
                    dd.Cells[8].Value = dueDate.ToString("yyyy-MM-dd");
                    button2.Enabled = true;
                }
            }
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClearScreen();
            _vismaClient.CloseDatabase();
        }

        private void switchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to switch to Direct Payment section?\n\n Current form data will be lost", "Proceed!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                _vismaClient.CloseDatabase();
                this.Hide();
                frmDirectPayments frmDirectPayments = new frmDirectPayments();
                frmDirectPayments.Show();
            }
        }
    }
}