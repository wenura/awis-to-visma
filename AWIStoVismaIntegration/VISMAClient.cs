﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AWIStoVismaIntegration
{
    public class VISMAClient
    {
        public int getRecentInvoiceNo()
        {
            try
            {
                int tmpInvoiceNo = 0;
                Common common = new Common();
                var result = XORBase5.DatabaseOpen(Common.countryDBName);
                if (result == XORBase5.DbOk)
                {
                    var res = XORBase5.RecordFirst(XORBase5.SERIES, 1);

                    int tmpID = 0;
                    int tmpPrintNum = 0;
                    int tmpInvoiceNoLast = 0;
                    IntPtr fYearNo = XORBase5.RecordField(XORBase5.SERIES, "YEARNO");
                    IntPtr fId = XORBase5.RecordField(XORBase5.SERIES, "ID");
                    IntPtr fPrintNum = XORBase5.RecordField(XORBase5.SERIES, "PRINTNUM");
                    IntPtr myFieldLastNum = XORBase5.RecordField(XORBase5.SERIES, "LASTNUM");
                    if (res == XORBase5.DbOk)
                    {
                        while (true)
                        {
                            tmpID = XORBase5.FieldValueLong(fId);
                            tmpPrintNum = XORBase5.FieldValueLong(fPrintNum);
                            if (tmpID == 101)
                            {
                                tmpInvoiceNoLast = XORBase5.FieldValueLong(myFieldLastNum);
                                if (tmpPrintNum == 1)
                                {
                                    tmpInvoiceNo = XORBase5.FieldValueLong(myFieldLastNum);
                                    XORBase5.DatabaseClose();
                                    return tmpInvoiceNo;
                                }
                            }
                            if (XORBase5.RecordNext(XORBase5.SERIES) != XORBase5.DbOk)
                            {
                                tmpInvoiceNo = tmpInvoiceNoLast;
                                XORBase5.DatabaseClose();
                                return tmpInvoiceNo;
                            }
                        }
                    }
                    else
                    {
                        common.AddVISMAErrorLog(result.ToString(), MethodBase.GetCurrentMethod().Name + " -  DatabaseOpen");
                    }

                }
                else 
                {
                    common.AddVISMAErrorLog(result.ToString(), MethodBase.GetCurrentMethod().Name + " -  RecordFirst");
                }
                return tmpInvoiceNo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool PersonFind(string personId)
        {
            try
            {
                Common common = new Common();
                var result = XORBase5.DatabaseOpen(Common.countryDBName);
                if (result == XORBase5.DbOk)
                {
                    if (XORBase5.PersonFind(XORBase5.CUSTOM, personId) > 0)
                    {
                        XORBase5.DatabaseClose();
                        return true;
                    }
                    else
                    {
                        XORBase5.DatabaseClose();
                        return false;
                    }
                }
                else
                {
                    common.AddVISMAErrorLog(result.ToString(), MethodBase.GetCurrentMethod().Name + " - DatabaseOpen");
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddPerson(CustomerViewModel customer)
        {
            try
            {
                Common common = new Common();
                var result = XORBase5.DatabaseOpen(Common.countryDBName);
                if (result == XORBase5.DbOk)
                {
                    int res = XORBase5.PersonAdd(XORBase5.CUSTOM, customer.PersonNumber, customer.PersonName, customer.Group, customer.Address1, customer.Address2, customer.Address3, customer.Address4, customer.PhoneNumber, customer.FaxNumber, customer.Reference, customer.TermsOfPayment, customer.TermsOfDelivery, customer.WayOfDelivery);
                    int re3 = XORBase5.RecordUpdate(XORBase5.PERSON);
                    XORBase5.DatabaseClose();
                }
                else
                {
                    common.AddVISMAErrorLog(result.ToString(), MethodBase.GetCurrentMethod().Name + " - DatabaseOpen");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddInvoice(string customerId, DateTime invoiceDate)
        {
            try
            {
                Common common = new Common();
                var result = XORBase5.DatabaseOpen(Common.countryDBName);
                if (result == XORBase5.DbOk)
                {
                    int invResult = XORBase5.InvoiceOpen(customerId, XORBase5.XCUSTINVOICE, XORBase5.NOTPRINTED, Convert.ToInt32(invoiceDate.Year), Convert.ToInt32(invoiceDate.Month), Convert.ToInt32(invoiceDate.Date), (double)0, (double)0);
                    return invResult;
                }
                else
                {
                    common.AddVISMAErrorLog(result.ToString(), MethodBase.GetCurrentMethod().Name + " - DatabaseOpen");
                    return result;
                }
                   
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
    }
}
