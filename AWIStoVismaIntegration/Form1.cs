﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Configuration;
using AWIStoVismaIntegration.Business;

namespace AWIStoVismaIntegration
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            string userName = textBox1.Text;
            string password = textBox2.Text;
            try
            {
                if (userName == string.Empty)
                {
                    MessageBox.Show("Username is Required", "AWIS to VISMA", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textBox1.Focus();
                    return;
                }
                else if (password == string.Empty)
                {
                    MessageBox.Show("Password is Required", Constant.appName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textBox2.Focus();
                    return;
                }
                else
                {
                    AWISClient client = new AWISClient();
                    if (client.Login(userName, password))
                    {
                    Common com = new Common();
                        com.FeedVISMAErrorDictionary();

                        FormCommonInterface commonInterface = new FormCommonInterface();
                        this.Hide();
                        commonInterface.Closed += (s, args) => this.Close();
                        commonInterface.Show();
                    }
                    else
                    {
                        MessageBox.Show("Invalid Username or Password", Constant.appName, MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1.PerformClick();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}