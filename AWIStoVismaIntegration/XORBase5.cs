//
//    Visma Compact 5 API
//
//    Copyright (C) 1996-2010, Visma Spcs AB
//    
//   $Id: XORBase5.cs,v 1.21.4.1 2009/10/23 06:44:37 VSW\Dahn_Nil Exp $
//
// Import module for interfacing C# applications to XORBase5.dll
//

using System;
using System.Text;
using System.Runtime.InteropServices;

public static class XORBase5
{
    // Database return codes (errors)
    public const int DbError = -1;            // General error
    public const int DbOk = 0;                // OK, no error
    public const int DbNotFound = 1;          // Record not found
    public const int DbEof = 2;               // End of file
    public const int DbBof = 3;               // Beginning of file
    public const int DbDuplicate = 4;         // Primary key already exists

    // Constants for person types
    public const int CUSTOM = 1;
    public const int SUPPLY = 2;

    // Constants for number types
    public const int XORDER = 0;
    public const int XCUSTINVOICE = 1;
    public const int XPURCHASE = 2;
    public const int XSUPPINVOICE = 3;
    public const int XPAY = 4;

    // Constants for invoice status
    public const int NOTPRINTED = 0;
    public const int PRINTED = 1;
    public const int TRANSFERED = 2;
    public const int PERMANENT = 3;
    public const int NOTREADY = 4;

    // Constants for ledger types
    public const int FA = 0;
    public const int KN = 1;
    public const int RF = 2;
    public const int AC = 3;
    public const int BE = 4;

    // Constants for XORBASE tables
    public const int CLIENT = 0;
    public const int ACCOUNT = 1;
    public const int ACCOUNTINGOBJECT = 2;        // Used to be OBJECT
    public const int BALANCE = 3;
    public const int VOUCHER = 4;
    public const int AUTOCALC = 5;                // Used to be AUTO
    public const int DEFAULTACCOUNT = 6;          // Used to be DEFAULT
    public const int TEXT = 7;
    public const int HISTORY = 8;
    public const int PERSON = 9;
    public const int PERSSALES = 10;
    public const int LEDGER = 11;
    public const int INVOICE = 12;
    public const int INVOICEROW = 13;
    public const int ARTICLE = 14;
    public const int ARTSALES = 15;
    public const int ACCYEAR = 16;
    public const int SERIES = 17;
    public const int RELATION = 18;
    public const int ACCCHART = 19;
    public const int SETTING = 20;
    public const int PERSSETTINGS = 21;
    public const int DOCLOG = 22;
    public const int ACCRUAL = 23;

    // Constants for default (reserved) accounts
    public const int DEFVATOUT = 1;               // Sales VAT
    public const int DEFVATIN = 10;               // Purchase VAT
    public const int DEFSTOCK = 18;               // Stock
    public const int DEFYEARRESULT = 19;          // Year results
    public const int DEFCUSTOMER = 20;            // Accounts receivable
    public const int DEFFREIGHT = 21;             // Charged freights
    public const int DEFFEE = 22;                 // Invoice fees
    public const int DEFADJUSTMENT = 23;          // Rounding
    public const int DEFINTERIN = 24;             // Interest income
    public const int DEFFINANCE = 25;             // Financial entries
    public const int DEFSALES = 26;               // Sales
    public const int DEFPURCHASE = 27;            // Purchase
    public const int DEFSUPPLIER = 28;            // Accounts payable
    public const int DEFPAYIN = 29;               // Accounts receivable (bank)
    public const int DEFCASH = 30;                // Cash
    public const int DEFINTEROUT = 31;            // Interest expenses
    public const int DEFCHANGE = 32;              // Adjustments
    public const int DEFDISCOUNTOUT = 33;         // Discount out
    public const int DEFDISCOUNTIN = 34;          // Discount in
    public const int DEFXPROFIT = 35;             // Exchange income
    public const int DEFXLOSS = 36;               // Exchange loss
    public const int DEFEXPSALES = 37;            // Export sales
    public const int DEFEXPSALES2 = 38;           // Import purchases
    public const int DEFPAYOUT = 39;              // Accounts payable (bank )
    public const int DEFVAT = 40;                 // VAT settle account
    public const int DEFFREIGHTEXP = 41;          // Export freight account
    public const int DEFFEEEXP = 42;              // Export fee account
    public const int DEFYEARRESULTBOOK = 43;      // Year Result (result)
    public const int DEFFREIGHTEU = 44;           // Export freight account
    public const int DEFFEEEU = 45;               // Export fee account
    public const int DEFREVVAT = 46;              // Default account for reversed VAT
    public const int DEFACCRUEDRECEIVABLE = 47;   // Default account for accrued receivable
    public const int DEFACCRUEDDEBT = 48;         // Default account for accrued debt


#region "API functions"

    public delegate int ErrorHandler(int ErrorNumber1, int ErrorNumber2, string ErrorString);

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int DatabaseOpen(string ClientNumber);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern void DatabaseClose();
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern void DatabaseSetPath(string Path);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int DatabaseGetPath(StringBuilder Path, int StringSize);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int DatabaseTableCount();
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern void SetErrorHandler(ErrorHandler ErrorCallback);

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int TableFieldCount(int TableIndex);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int TableRecordCount(int TableIndex);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int TableName(StringBuilder Path, int StringSize);

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int RecordCreate(int TableIndex);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int RecordFind(IntPtr Key);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int RecordFirst(int TableIndex, int KeyNumber);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int RecordNext(int TableIndex);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int RecordPrev(int TableIndex);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int RecordLast(int TableIndex, int KeyNumber);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int RecordUpdate(int TableIndex);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int RecordAdd(int TableIndex);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int RecordDelete(int TableIndex);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern IntPtr RecordField(int TableIndex, string FieldName);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern IntPtr RecordKey(int TableIndex, int KeyNumber);

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int FieldLength(IntPtr Field);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int FieldType(IntPtr Field);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int FieldName(int TableIndex, int FieldIndex, StringBuilder StringBuffer, int StringSize);

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int FieldAssignDouble(IntPtr Field, double value);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int FieldAssignLong(IntPtr Field, int value);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int FieldAssignString(IntPtr Field, string value);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int FieldAssignBool(IntPtr Field, bool value);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int FieldAssignDate(IntPtr Field, short value);

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern double FieldValueDouble(IntPtr Field);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int FieldValueLong(IntPtr Field);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern bool FieldValueBool(IntPtr Field);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern short FieldValueDate(IntPtr Field);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int FieldValueString(IntPtr Field, StringBuilder StringBuffer, int StringSize);

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern IntPtr KeyField(IntPtr Key, string FieldName);

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int InvoiceFind(int serieNumber, int invoiceIndex, int invoiceNumber);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int InvoiceRowFind(int serieNumber, int invoiceIndex, int invoiceNumber, int rowNumber);

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int InvoiceOpen(string PersNumber, int type, int Status, int Year, int Month, int Day, double Freight, double Fee);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int InvoiceAddRow(string ArticleID, string ArticleName, string AccountNumber, string Object1, string Object2, string Unit, double Items, double Price, double Discount);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int InvoiceClose();

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int VoucherFind(int yearNumber, int series, int voucherNumber, int rowNumber);

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int VoucherOpen(int Series, int Year, int Month, int Day);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int VoucherAddRow(string AccountNumber, string VoucherText, string Object1, string Object2, double amount);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int VoucherClose();

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int LedgerFind(int serieNumber, int personType, int ledgerType, int ledgerNumber);

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int LedgerAddCustomerInvoice(string personNumber, int year, int month, int day, double amount, double vat, int ledType, int invoiceNumber, int update, string account);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int LedgerAddSupplierInvoice(string personNumber, string suppliersInvoiceNo, int year, int month, int day, double amount, double vat, int ledType, int update, string account);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern void DocumentAddSupplierInvoice(string filePath, int storeDocument);

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int GetReservedAccount(int DefaultNumber, StringBuilder AccountNumber, int StringSize);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int GetStandardText(int TextNumber, StringBuilder TextString, int StringSize);

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int PersonFind(int personType, string personID);

    // Add a person
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int PersonAdd( 
        int PersonType, 
        string PersonNumber, 
        string PersonName, 
        string Group, 
        string Address1, 
        string Address2, 
        string Address3, 
        string Address4, 
        string PhoneNumber, 
        string FaxNumber, 
        string Reference, 
        int TermsOfPayment,
        int TermsOfDelivery,
        int WayOfDelivery);

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int ArticleFind(string ArticleNumber);

    // Add an article
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int ArticleAdd(
        string ArticleNumber, 
        string ArticleName, 
        string Group, 
        double ItemsInStock,
        double OrderPoint,
        double Price,
        double PurchasePrice);

    // Find an object
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int ObjectFind(string ObjectID);

    // Add an object
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int ObjectAdd(
        int objectType,
        string ObjectID,
        string Name,
        int SpanYear);

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern short DueDateCalc(string TermsOfPayment, short StartDate);

#region "Date functions"
    // -----------------------------------
    // Date functions
    // -----------------------------------
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern short DateSystem();
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern short YMDToDate(int year, int mon, int day, int check);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern short FirstDayOfWeek(int year);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern short DayOfWeek(short julianDate);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern short BeginWeek(short julianDate);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern short EndWeek(short julianDate);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern short EndWorkWeek(short julianDate);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern short DayOfYear(short julianDate);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern short BeginYear(short julianDate);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern short EndYear(short julianDate);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern short BeginMon(short julianDate);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern short EndMon(short julianDate);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int Year(short julianDate);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int Month(short julianDate);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int Day(short julianDate);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int Week(short julianDate);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int GetYearNo(short julianDate);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int IsDateLocked(short julianDate);

#endregion

#region "Misc functions"

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int ExportSieFile(string FileName, int Level, short julianDate);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int ImportSieFile(string FileName);

    [StructLayout(LayoutKind.Sequential, CharSet=CharSet.Ansi, Pack=4)]
    public struct ClientNameType
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst=16)] public string id;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst=48)] public string name;
    }

    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int ClientListInit([Out,In] ref ClientNameType clientName);
    [DllImport("XORBASE5.DLL", CharSet=CharSet.Ansi)] public static extern int ClientListNext([Out,In] ref ClientNameType clientName);

#endregion

#endregion

}
