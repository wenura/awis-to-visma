﻿namespace AWIStoVismaIntegration
{
    partial class FormCommonInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCommonInterface));
            this.btnInvoice = new System.Windows.Forms.Button();
            this.btnDirectPayments = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnInvoice
            // 
            this.btnInvoice.Location = new System.Drawing.Point(15, 12);
            this.btnInvoice.Name = "btnInvoice";
            this.btnInvoice.Size = new System.Drawing.Size(250, 237);
            this.btnInvoice.TabIndex = 0;
            this.btnInvoice.Text = "Invoice";
            this.btnInvoice.UseVisualStyleBackColor = true;
            this.btnInvoice.Click += new System.EventHandler(this.btnInvoice_Click);
            // 
            // btnDirectPayments
            // 
            this.btnDirectPayments.Location = new System.Drawing.Point(312, 12);
            this.btnDirectPayments.Name = "btnDirectPayments";
            this.btnDirectPayments.Size = new System.Drawing.Size(250, 237);
            this.btnDirectPayments.TabIndex = 1;
            this.btnDirectPayments.Text = "Direct Payments";
            this.btnDirectPayments.UseVisualStyleBackColor = true;
            this.btnDirectPayments.Click += new System.EventHandler(this.btnDirectPayments_Click);
            // 
            // FormCommonInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 263);
            this.Controls.Add(this.btnDirectPayments);
            this.Controls.Add(this.btnInvoice);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormCommonInterface";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Common Interface";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormCommonInterface_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnInvoice;
        private System.Windows.Forms.Button btnDirectPayments;
    }
}