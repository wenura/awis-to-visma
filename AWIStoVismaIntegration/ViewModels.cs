﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWIStoVismaIntegration
{
    public class LoginViewModel
    {
        public string grant_type { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string client_id { get; set; }
    }

    public class LoginResult
    {
        public string access_token {get;set;}
        public string token_type{get;set;}
        public int expires_in{get;set;}
        public string refresh_token{get;set;}
        public string client_id{get;set;}
        public string userName{get;set;}
        public string refreshToken_timeout {get;set;}
        public DateTime issued {get;set;}
        public DateTime expires {get;set;}
    }

    public class CountryViewModel
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class InvoicesViewModel
    {
        public Nullable<int> CustomerId { get; set; }
        public String CustomerName { get; set; }
        public String Type { get; set; }
        public String PaymentTerm { get; set; }
        public int InvoiceNumber { get; set; }
        public Nullable<Decimal> GrossValue { get; set; }
        public Nullable<Decimal> TaxValue { get; set; }
        public Nullable<Decimal> NETValue { get; set; }
        public String InvoiceDate { get; set; }
        public String DueDate { get; set; }
        public String Country { get; set; }
        public Nullable<Int32> InvoicePeriod { get; set; }
        public Nullable<Int32> ActionId { get; set; }
        
    }

    public class CustomerViewModel
    {
        public int PersonType { get; set; } 
        public string PersonNumber { get; set; } 
        public string PersonName { get; set; } 
        public string Group { get; set; } 
        public string Address1 { get; set; } 
        public string Address2 { get; set; } 
        public string Address3 { get; set; } 
        public string Address4 { get; set; } 
        public string PhoneNumber { get; set; } 
        public string FaxNumber { get; set; } 
        public string Reference { get; set; } 
        public int TermsOfPayment { get; set; }
        public int TermsOfDelivery { get; set; }
        public int WayOfDelivery { get; set; }
    }

    public class InvoicesForVismaViewModel
    {
        public List<InvoicesViewModel> Invoices { get; set; }
        public decimal TotalNetValue { get; set; }
        public decimal TotalGrossValue { get; set; }
        public decimal TotalTaxValue { get; set; }
    }
}
