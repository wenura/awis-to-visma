﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWIStoVismaIntegration
{
    public class Common
    {
        public static string countryDBName = string.Empty;

        public void AddErrorLog(Exception ex)
        {
            File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", string.Format(Environment.NewLine + DateTime.Now + " Error Occured" + ex.Message.ToString(), Environment.NewLine + (ex.InnerException != null ? ex.InnerException.Message : string.Empty), Environment.NewLine));
        }

        public void AddVISMAErrorLog(string errorCode, string methodName)
        {
            File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", string.Format(Environment.NewLine + DateTime.Now + " - " + methodName + " - " + errorCode));
        }
    }
}
