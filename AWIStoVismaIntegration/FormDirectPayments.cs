﻿using AWIStoVismaIntegration.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AWIStoVismaIntegration
{
    public partial class frmDirectPayments : Form
    {
        #region Properties

        AWISClient awisClient;
        VISMAClient vismaClient;
        Common common;

        #endregion Properties

        #region Methods

        public frmDirectPayments()
        {
            InitializeComponent();
            LoadInitialData();
            GetLastUpdatedData();
        }

        private void LoadInitialData()
        {
            awisClient = new AWISClient();
            vismaClient = new VISMAClient();
            common = new Common();

            cmbCountry.DropDownStyle = ComboBoxStyle.DropDownList;
            List<CountryViewModel> countries = new List<CountryViewModel>();
            countries = awisClient.GetCountryList();
            cmbCountry.ValueMember = "id";
            cmbCountry.DisplayMember = "name";

            CountryViewModel testCountry = new CountryViewModel();
            testCountry.id = Constant.TESTBOLAG_ID;
            testCountry.name = "TESTBOLAG";
            countries.Add(testCountry);

            if (countries.Count > 0)
            {
                vismaClient.CloseDatabase();
                cmbCountry.DataSource = countries;

                toolStripLabel.Text = "Online";
                toolStripLabel.ForeColor = Color.Green;
                toolStripLabelPath.Text = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                timerPayment = new Timer();
                timerPayment.Tick += new EventHandler(timerPayment_Tick);
                timerPayment.Interval = 10000;
                timerPayment.Start();
            }
        }

        private void GetLastUpdatedData()   
        {
            try
            {
                int lastVoucherNo = 0;
                if (Int32.Parse(cmbCountry.SelectedValue.ToString()) > 0)
                    awisClient.GetCountryDBName(Int32.Parse(cmbCountry.SelectedValue.ToString()));
                else
                    Common.countryDBName = Constant.testDB;

                int dbOpenResult;
                dbOpenResult = vismaClient.DatabaseOpen();

                if (dbOpenResult == XORBase5.DbOk)
                {
                    lastVoucherNo = vismaClient.getRecentVouchereNo();
                    vismaClient.CloseDatabase();

                    if (lastVoucherNo > 0)
                    {
                        txtLastNo.Text = lastVoucherNo.ToString();
                    }
                    else
                    {
                        txtLastNo.Text = "-";
                    }
                }
                else
                {
                    txtLastNo.Text = "-";
                }

                LastUpdatedData data = awisClient.GetLastUpdatedData(cmbCountry.SelectedValue.ToString());
                txtLastDate.Text = data.LastUpdatedDate;

                dtpFrom.MinDate = DateTimePicker.MinDateTime;
                dtpFrom.MaxDate = DateTimePicker.MinDateTime;

                dtpTo.MinDate = DateTimePicker.MinDateTime;
                dtpTo.MaxDate = DateTimePicker.MinDateTime;

                if (string.IsNullOrEmpty(data.LastUpdatedDate))
                {
                    dtpFrom.MaxDate = DateTime.ParseExact(ConfigurationManager.AppSettings["ReleasedDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(Constant.MaxDateCount);
                    dtpFrom.MinDate = DateTime.ParseExact(ConfigurationManager.AppSettings["ReleasedDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    dtpTo.MaxDate = DateTime.ParseExact(ConfigurationManager.AppSettings["ReleasedDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(Constant.MaxDateCount);
                    dtpTo.MinDate = DateTime.ParseExact(ConfigurationManager.AppSettings["ReleasedDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    txtLastDate.Text = "-";
                }
                else
                {
                    dtpFrom.MaxDate = DateTime.ParseExact(data.LastUpdatedDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(Constant.MaxDateCount);
                    dtpFrom.MinDate = DateTime.ParseExact(data.LastUpdatedDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);

                    dtpTo.MaxDate = DateTime.ParseExact(data.LastUpdatedDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(Constant.MaxDateCount);
                    dtpTo.MinDate = DateTime.ParseExact(data.LastUpdatedDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).AddDays(1);
                }

                dtpFrom.Value = dtpFrom.MinDate;
                dtpTo.Value = dtpFrom.MaxDate;
            }
            catch(Exception ex)
            {

            }
        }

        private void ClearScreen()
        {
            txtLastNo.Text = string.Empty;
            txtLastDate.Text = string.Empty;
            //txtGross.Text = string.Empty;
            //txtTax.Text = string.Empty;
            //txtNet.Text = string.Empty;

            GetLastUpdatedData();

            dgvPayment.DataSource = null;

            //toolStripProgressBar1.Value = 1;
            //toolStripProgressBar1.Visible = false;
            //button2.Enabled = true;
            //GetLastInvoiceNo((int)comboBox1.SelectedValue);
        }

        private void SearchPaymentData()
        {
            try
            {
                DirectPaymentsForVismaViewModel payments = new DirectPaymentsForVismaViewModel();
                string fromInvoiceNo = string.Empty;
                string toInvoiceNo = string.Empty;
                awisClient.GetCountryDBName(Int32.Parse(cmbCountry.SelectedValue.ToString()));

                DateTime fromDate = dtpFrom.Value != null ? dtpFrom.Value : DateTime.Now;
                DateTime toDate = dtpTo.Value != null ? dtpTo.Value : DateTime.Now;

                int country = (int)cmbCountry.SelectedValue != 0 ? (int)cmbCountry.SelectedValue : Constant.defaultCountry;

                payments = awisClient.GetDirectPaymentsList(fromDate, toDate, country);

                int paymentsCount = payments != null && payments.DirectPayments != null ? payments.DirectPayments.Count : 0;

                if (paymentsCount > 0)
                {
                    dgvPayment.DataSource = payments.DirectPayments;
                    dgvPayment.AutoGenerateColumns = false;

                    dgvPayment.Columns[0].Name = "Date";
                    dgvPayment.Columns[0].HeaderText = "Date";
                    dgvPayment.Columns[0].DataPropertyName = "PaymentDate";
                    dgvPayment.Columns[1].HeaderText = "Account";
                    dgvPayment.Columns[1].DataPropertyName = "AccountNumber";
                    dgvPayment.Columns[2].HeaderText = "Text";
                    dgvPayment.Columns[2].DataPropertyName = "VoucherText";
                    dgvPayment.Columns[3].HeaderText = "Debit";
                    dgvPayment.Columns[3].DataPropertyName = "ShowDebitAmount";
                    dgvPayment.Columns[4].HeaderText = "Credit";
                    dgvPayment.Columns[4].DataPropertyName = "ShowCreditAmount";
                    dgvPayment.Columns[5].HeaderText = "YYMM";
                    dgvPayment.Columns[5].DataPropertyName = "Period";

                    dgvPayment.Columns[6].Visible = false;
                    dgvPayment.Columns[7].Visible = false;
                    dgvPayment.Columns[8].Visible = false;
                    dgvPayment.Columns[9].Visible = false;
                    dgvPayment.Columns[10].Visible = false;
                    dgvPayment.Columns[11].Visible = false;
                    dgvPayment.Columns[12].Visible = false;
                    dgvPayment.Columns[13].Visible = false;
                    dgvPayment.Columns[14].Visible = false;
                    dgvPayment.Columns[15].Visible = false;
                    dgvPayment.Columns[16].Visible = false;
                    dgvPayment.Columns[17].Visible = false;
                    dgvPayment.Columns[18].Visible = false;

                    string grossValue = payments.DirectPayments[0].GrossValue.ToString();
                    //txtGross.Text = payments.TotalGrossValue.ToString();
                    //txtTax.Text = payments.TotalTaxValue.ToString();
                    //txtNet.Text = payments.TotalNetValue.ToString();                    
                }
                else
                {
                    MessageBox.Show("No direct payments found!", Constant.appName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgvPayment.DataSource = null;
                    return;
                }
            }
            catch (Exception ex)
            {
                btnSearch.Enabled = true;
                awisClient.AddErrorToAWIS(Constant.userName, ex);
                common.AddErrorLog(ex);
                MessageBox.Show(Constant.exceptionError, "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion Methods

        #region Events
        private void frmDirectPayments_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to close this window?", Constant.appName, MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                vismaClient.CloseDatabase();
                e.Cancel = false;
                Application.ExitThread();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void switchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to switch to Invoice section?\n\n Current form data will be lost", "Proceed!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                vismaClient.CloseDatabase();
                this.Hide();
                Form2 frmInvoice = new Form2();
                frmInvoice.Show();
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Loading loading = new Loading();
            loading.Show();
            this.Enabled = false;            

            if (dtpFrom.Value <= dtpTo.Value)
            {
                SearchPaymentData();

                this.Enabled = true;
                loading.Hide();
            }
            else
            {
                this.Enabled = true;
                loading.Hide();

                MessageBox.Show("From date can’t be greater than the To date", Constant.appName, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClearScreen();
            vismaClient.CloseDatabase();
        }

        private void timerPayment_Tick(object sender, EventArgs e)
        {
            bool isOnline = awisClient.TestConnection();
            if (isOnline)
            {
                toolStripLabel.Text = "Online";
                toolStripLabel.ForeColor = Color.Green;
            }
            else
            {
                toolStripLabel.Text = "Offline";
                toolStripLabel.ForeColor = Color.Red;
            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            Loading loading = new Loading();

            if (dgvPayment.RowCount > 0)
            {
                DialogResult result = MessageBox.Show("Are you sure you want to proceed?\n\nNote: This will feed direct payment lines to VISMA", "Proceed!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {                    
                    loading.Show();
                    this.Enabled = false;

                    ProceedDirectPayments();

                    this.Enabled = true;
                    loading.Hide();
                }
            }
            else
            {
                this.Enabled = true;
                loading.Hide();

                MessageBox.Show("Payment List is empty", "Proceed!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void ProceedDirectPayments()
        {
            try
            {
                List<DirectPaymentsViewModel> payments = new List<DirectPaymentsViewModel>();
                Result result = new Result();
                payments = (List<DirectPaymentsViewModel>)dgvPayment.DataSource;
                result = vismaClient.ProceedDirectPayments(payments, Constant.userName);

                if (result.isSuccess == true)
                {
                    MessageBox.Show("Direct payments process successfully completed!", Constant.appName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ClearScreen();
                    GetLastUpdatedData();
                }
                else
                {
                    MessageBox.Show(result.message, "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    btnProcess.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            Loading loading = new Loading();

            loading.Show();
            this.Enabled = false;

            ClearScreen();

            this.Enabled = true;
            loading.Hide();
        }

        #endregion Events


    }
}