﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AWIStoVismaIntegration.Business
{
    public class Constant
    {
        public static string vismaError = "Error has occurred. Check error log";
        public static string exceptionError = "Error has occurd. Please contact AutoConcept support team.";
        public static string invoiceNoNotFound = "Cannot find last invoice No. for the selected country.";
        public static string appName = "AWIS to VISMA";
        public static string dbError = "Database open failed. Please contact AutoConcept support team.";

        public static Dictionary<string, Dictionary<int, string>> vismaMethods = new Dictionary<string, Dictionary<int, string>>();
        public static string testDB = "TESTBOLAG";
        public static int defaultCountry = 6;
        public static string userName = string.Empty;
        public static int directPaymentSeries = Int32.Parse(ConfigurationManager.AppSettings["DirectPaymentSeries"].ToString());
        public static int MaxDateCount = 30;
        public static int TESTBOLAG_ID = Int32.Parse(ConfigurationManager.AppSettings["TESTBOLAG_ID"].ToString());
    }
    public enum VismaMethods
    {
        DatabaseOpen,
        PersonFind,
        PersonAdd,
        InvoiceOpen,
        InvoiceAddRow,
        InvoiceClose,
        DirectPaymentOpen,
        DirectPaymentAddRow,
        DirectPaymentClose
    }

    public enum PaymentType
    {
        Credit = 1,
        Debit = 2
    }
}
