﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AWIStoVismaIntegration.Business
{
    public class VISMAClient
    {
        Common common = new Common();
        AWISClient _awisClient = new AWISClient();
        Common _common = new Common();

        public int getRecentInvoiceNo()
        {
            try
            {
                int tmpInvoiceNo = 0;
                Common common = new Common();
                var res = XORBase5.RecordFirst(XORBase5.SERIES, 1);

                int tmpID = 0;
                int tmpPrintNum = 0;
                int tmpInvoiceNoLast = 0;
                IntPtr fYearNo = XORBase5.RecordField(XORBase5.SERIES, "YEARNO");
                IntPtr fId = XORBase5.RecordField(XORBase5.SERIES, "ID");
                IntPtr fPrintNum = XORBase5.RecordField(XORBase5.SERIES, "PRINTNUM");
                IntPtr myFieldLastNum = XORBase5.RecordField(XORBase5.SERIES, "LASTNUM");
                if (res == XORBase5.DbOk)
                {
                    while (true)
                    {
                        tmpID = XORBase5.FieldValueLong(fId);
                        tmpPrintNum = XORBase5.FieldValueLong(fPrintNum);
                        if (tmpID == 101)
                        {
                            tmpInvoiceNoLast = XORBase5.FieldValueLong(myFieldLastNum);
                            if (tmpPrintNum == 1)
                            {
                                tmpInvoiceNo = XORBase5.FieldValueLong(myFieldLastNum);
                                return tmpInvoiceNo;
                            }
                        }
                        if (XORBase5.RecordNext(XORBase5.SERIES) != XORBase5.DbOk)
                        {
                            tmpInvoiceNo = tmpInvoiceNoLast;
                            return tmpInvoiceNo;
                        }
                    }
                }
                else
                {
                    common.AddVISMALog(MethodBase.GetCurrentMethod().Name + " - RecordFirst : " + res.ToString());
                }
                return tmpInvoiceNo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int getRecentVouchereNo()
        {
            try
            {
                int tmpInvoiceNo = 0;
                Common common = new Common();
                var res = XORBase5.RecordFirst(XORBase5.SERIES, 1);

                int tmpID = 0;
                int tmpPrintNum = 0;
                int tmpInvoiceNoLast = 0;
                IntPtr fYearNo = XORBase5.RecordField(XORBase5.SERIES, "YEARNO");
                IntPtr fId = XORBase5.RecordField(XORBase5.SERIES, "ID");
                IntPtr fPrintNum = XORBase5.RecordField(XORBase5.SERIES, "PRINTNUM");
                IntPtr myFieldLastNum = XORBase5.RecordField(XORBase5.SERIES, "LASTNUM");

                if (res == XORBase5.DbOk)
                {
                    while (true)
                    {
                        tmpID = XORBase5.FieldValueLong(fId);
                        tmpPrintNum = XORBase5.FieldValueLong(fPrintNum);
                        if (tmpID == Constant.directPaymentSeries)
                        {
                            tmpInvoiceNoLast = XORBase5.FieldValueLong(myFieldLastNum);
                            if (tmpPrintNum == 1)
                            {
                                tmpInvoiceNo = XORBase5.FieldValueLong(myFieldLastNum);
                                return tmpInvoiceNo;
                            }
                        }
                        if (XORBase5.RecordNext(XORBase5.SERIES) != XORBase5.DbOk)
                        {
                            tmpInvoiceNo = tmpInvoiceNoLast;
                            return tmpInvoiceNo;
                        }
                    }
                }
                else
                {
                    common.AddVISMALog(MethodBase.GetCurrentMethod().Name + " - RecordFirst : " + res.ToString());
                }
                return tmpInvoiceNo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Result ProceedInvoiceGeneration(List<InvoicesViewModel> invoices, string user, ref ToolStripProgressBar toolStripProgressBar1)
        {
            Common common = new Common();
            Result result = new Result();
            result.isSuccess = true;
            try
            {
                _common.AddVISMALog("Invoice integration process Start");
                string invoiceNos = string.Empty;
                int dbOpenResult;

                dbOpenResult = this.DatabaseOpen();
                if (dbOpenResult == XORBase5.DbOk)
                {
                    toolStripProgressBar1.Visible = true;
                    toolStripProgressBar1.Minimum = 1;
                    toolStripProgressBar1.Maximum = invoices.Count;
                    toolStripProgressBar1.Value = 1;
                    toolStripProgressBar1.Step = 1;

                    int invoiceCount = invoices.Count;
                    int index = 0;
                    foreach (InvoicesViewModel invoice in invoices)
                    {
                        index += 1;
                        if (!this.PersonFind(Convert.ToString(invoice.CustomerId)))
                        {
                            CustomerViewModel customer = new CustomerViewModel();
                            customer = _awisClient.GetCustomerDetail(invoice.CustomerId.Value);
                            customer.TermsOfPayment = Convert.ToInt32(ConfigurationManager.AppSettings[invoice.PaymentTerm]);

                            var cusAddResult = this.AddPerson(customer);
                            if (cusAddResult < 0)
                            {
                                result.isSuccess = false;
                                result.message = invoice.InvoiceNumber.ToString() + " - " + _common.GetVISMAError(cusAddResult, VismaMethods.PersonAdd);
                                break;
                            }
                        }
                        DateTime invoiceDate = DateTime.ParseExact(invoice.InvoiceDate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                        int invResult = this.AddInvoice(Convert.ToString(invoice.CustomerId), invoiceDate);
                        if (invResult > 0)
                        {
                            switch (invoice.Type)
                            {
                                case "STAMPELAVGIFT":
                                    InvoiceLineViewModel taxInvoiceLine = new InvoiceLineViewModel();
                                    taxInvoiceLine.ArticleID = "F";
                                    taxInvoiceLine.Discount = (double)0;
                                    taxInvoiceLine.Items = 1;
                                    taxInvoiceLine.Object1 = string.Empty;
                                    taxInvoiceLine.Object2 = string.Empty;
                                    taxInvoiceLine.Price = (double)invoice.GrossValue;
                                    taxInvoiceLine.Unit = "1";
                                    int taxInvoiceLineAddRes = this.AddInvoiceLine(taxInvoiceLine);
                                    if (taxInvoiceLineAddRes < 0)
                                    {
                                        result.isSuccess = false;
                                        result.message = invoice.InvoiceNumber.ToString() + " - " + _common.GetVISMAError(taxInvoiceLineAddRes, VismaMethods.InvoiceAddRow);
                                        //break;
                                    }

                                    InvoiceLineViewModel taxLine = new InvoiceLineViewModel();
                                    taxLine.ArticleID = "STAMPELAVGIFT";
                                    taxLine.Discount = (double)0;
                                    taxLine.Items = 1;
                                    taxLine.Object1 = string.Empty;
                                    taxLine.Object2 = string.Empty;
                                    taxLine.Price = (double)invoice.TaxValue;
                                    taxLine.Unit = "1";
                                    int taxLineAddRes = this.AddInvoiceLine(taxLine);
                                    if (taxLineAddRes < 0)
                                    {
                                        result.isSuccess = false;
                                        result.message = invoice.InvoiceNumber.ToString() + "" + _common.GetVISMAError(taxLineAddRes, VismaMethods.InvoiceAddRow);
                                    }
                                    break;

                                case "PROVISION":
                                    InvoiceLineViewModel prvInvLine = new InvoiceLineViewModel();
                                    prvInvLine.ArticleID = "F";
                                    prvInvLine.Discount = (double)0;
                                    prvInvLine.Items = 1;
                                    prvInvLine.Object1 = string.Empty;
                                    prvInvLine.Object2 = string.Empty;
                                    prvInvLine.Price = (double)invoice.GrossValue;
                                    prvInvLine.Unit = "1";
                                    int prvInvLineAddRes = this.AddInvoiceLine(prvInvLine);
                                    if (prvInvLineAddRes < 0)
                                    {
                                        result.isSuccess = false;
                                        result.message = invoice.InvoiceNumber.ToString() + " - " + _common.GetVISMAError(prvInvLineAddRes, VismaMethods.InvoiceAddRow);
                                    }

                                    InvoiceLineViewModel provisionLine = new InvoiceLineViewModel();
                                    provisionLine.ArticleID = "PROVISION";
                                    provisionLine.Discount = (double)0;
                                    provisionLine.Items = 1;
                                    provisionLine.Object1 = string.Empty;
                                    provisionLine.Object2 = string.Empty;
                                    provisionLine.Price = (double)invoice.Provision;
                                    provisionLine.Unit = "1";
                                    int prvLineAddRes = this.AddInvoiceLine(provisionLine);
                                    if (prvLineAddRes < 0)
                                    {
                                        result.isSuccess = false;
                                        result.message = invoice.InvoiceNumber.ToString() + " - " + _common.GetVISMAError(prvLineAddRes, VismaMethods.InvoiceAddRow);
                                        //break;
                                    }
                                    break;
                                default:
                                    InvoiceLineViewModel invoiceLine = new InvoiceLineViewModel();
                                    invoiceLine.ArticleID = invoice.Type;
                                    invoiceLine.Discount = (double)0;
                                    invoiceLine.Items = 1;
                                    invoiceLine.Object1 = string.Empty;
                                    invoiceLine.Object2 = string.Empty;
                                    invoiceLine.Price = (double)invoice.NETValue;
                                    invoiceLine.Unit = "1";

                                    int invLineAddRes = this.AddInvoiceLine(invoiceLine);
                                    if (invLineAddRes < 0)
                                    {
                                        result.isSuccess = false;
                                        result.message = invoice.InvoiceNumber.ToString() + " - " + _common.GetVISMAError(invLineAddRes, VismaMethods.InvoiceAddRow);
                                        //break;
                                    }
                                    break;
                            }

                            var invCloseRes = XORBase5.InvoiceClose();
                            if (invCloseRes < 0)
                            {
                                result.isSuccess = false;
                                result.message = invoice.InvoiceNumber.ToString() + " - " + _common.GetVISMAError(invCloseRes, VismaMethods.InvoiceClose);
                                break;
                            }
                        }
                        else
                        {
                            result.isSuccess = false;
                            result.message = invoice.InvoiceNumber.ToString() + " - " + _common.GetVISMAError(invResult, VismaMethods.InvoiceOpen);
                            break;
                        }
                        invoiceNos += invoice.InvoiceNumber + " , ";
                        toolStripProgressBar1.PerformStep();
                    }
                    this.CloseDatabase();
                    common.AddVISMALog("Invoice integration end.Created Invoices - " + invoiceNos);
                    return result;
                }
                else
                {
                    this.CloseDatabase();
                    result.isSuccess = false;
                    result.message = _common.GetVISMAError(dbOpenResult, VismaMethods.DatabaseOpen);
                    return result;
                }
            }
            catch (Exception ex)
            {
                this.CloseDatabase();
                _awisClient.AddErrorToAWIS(user, ex);
                _common.AddErrorLog(ex);
                result.isSuccess = false;
                result.message = Constant.exceptionError;
                return result;
            }
        }

        public bool PersonFind(string personId)
        {
            try
            {
                if (XORBase5.PersonFind(XORBase5.CUSTOM, personId) > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddPerson(CustomerViewModel customer)
        {
            try
            {
                int res = XORBase5.PersonAdd(XORBase5.CUSTOM, customer.PersonNumber, customer.PersonName, customer.Group, customer.Address1, customer.Address2, customer.Address3, customer.Address4, customer.PhoneNumber, customer.FaxNumber, customer.Reference, customer.TermsOfPayment, customer.TermsOfDelivery, customer.WayOfDelivery);
                //int res = XORBase5.PersonAdd(XORBase5.CUSTOM, customer.PersonNumber, customer.PersonName, customer.Group, string.Empty, string.Empty, string.Empty, string.Empty, customer.PhoneNumber, customer.FaxNumber, customer.Reference, 1, customer.TermsOfDelivery, customer.WayOfDelivery);

                int re3 = XORBase5.RecordUpdate(XORBase5.PERSON);
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddInvoice(string customerId, DateTime invoiceDate)
        {
            try
            {
                Common common = new Common();
                int invResult = XORBase5.InvoiceOpen(customerId, XORBase5.XCUSTINVOICE, XORBase5.NOTPRINTED, Convert.ToInt32(invoiceDate.Year), Convert.ToInt32(invoiceDate.Month), Convert.ToInt32(invoiceDate.Day), (double)0, (double)0);
                if (invResult < 0)
                {
                    common.AddVISMALog(MethodBase.GetCurrentMethod().Name + " - InvoiceOpen : " + invResult.ToString());
                }
                return invResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddInvoiceLine(InvoiceLineViewModel invoiceLine)
        {
            try
            {

                int result = XORBase5.InvoiceAddRow(invoiceLine.ArticleID, invoiceLine.ArticleName, invoiceLine.AccountNumber, invoiceLine.Object1, invoiceLine.Object2, invoiceLine.Unit, invoiceLine.Items, invoiceLine.Price, invoiceLine.Discount);
                if (result < 0)
                {
                    common.AddVISMALog(MethodBase.GetCurrentMethod().Name + " - InvoiceAddRow : " + result.ToString());
                }
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        #region Common

        public int DatabaseOpen()
        {
            try
            {
                var result = XORBase5.DatabaseOpen(Common.countryDBName);
                if (result != XORBase5.DbOk)
                    common.AddVISMALog(MethodBase.GetCurrentMethod().Name + " - DataBaseOpen : " + result.ToString());
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void CloseDatabase()
        {
            try
            {
                XORBase5.DatabaseClose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Direct Payments

        public Result ProceedDirectPayments(List<DirectPaymentsViewModel> payments, string user)
        {
            Common common = new Common();
            Result result = new Result();
            result.isSuccess = true;
            DateTime? rowDate = new DateTime();
            rowDate = null;
            DateTime? previousRowDate = new DateTime();
            previousRowDate = null;
            bool isPreviousClose = false;
            string previousCountryCode = string.Empty;

            try
            {
                common.AddVISMALog("Direct payments process started");
                string paymentNos = string.Empty;
                int dbOpenResult;
                int closeResult = 0;
                int openResult;
                int addResult;
                int rowCounter = 1;

                dbOpenResult = this.DatabaseOpen();

                if (dbOpenResult == XORBase5.DbOk)
                {
                    int paymentCount = payments.Count;

                    foreach (DirectPaymentsViewModel payment in payments)
                    {
                        if (rowDate == null)
                        {
                            rowDate = DateTime.ParseExact(payment.PaymentDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);

                            openResult = XORBase5.VoucherOpen(Constant.directPaymentSeries, rowDate.Value.Year, rowDate.Value.Month, rowDate.Value.Day);

                            addResult = XORBase5.VoucherAddRow(payment.AccountNumber, payment.VoucherText, string.Empty, string.Empty, payment.PaymentType == PaymentType.Debit.ToString() ? double.Parse(payment.DebitAmount.ToString()) : double.Parse(payment.CreditAmount.ToString()));
                        }
                        else if (rowDate == DateTime.ParseExact(payment.PaymentDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None))
                        {
                            rowDate = DateTime.ParseExact(payment.PaymentDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);

                            addResult = XORBase5.VoucherAddRow(payment.AccountNumber, payment.VoucherText, string.Empty, string.Empty, payment.PaymentType == PaymentType.Debit.ToString() ? double.Parse(payment.DebitAmount.ToString()) : double.Parse(payment.CreditAmount.ToString()));

                            if (paymentCount == rowCounter)
                            {
                                closeResult = XORBase5.VoucherClose();
                            }
                        }
                        else if (rowDate != null && rowDate != DateTime.ParseExact(payment.PaymentDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None))
                        {
                            closeResult = XORBase5.VoucherClose();
                            isPreviousClose = true;
                            previousCountryCode = payments[rowCounter - 2].CountryCode;

                            previousRowDate = DateTime.ParseExact(payments[rowCounter - 2].PaymentDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                            rowDate = DateTime.ParseExact(payment.PaymentDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);

                            openResult = XORBase5.VoucherOpen(Constant.directPaymentSeries, rowDate.Value.Year, rowDate.Value.Month, rowDate.Value.Day);

                            addResult = XORBase5.VoucherAddRow(payment.AccountNumber, payment.VoucherText, string.Empty, string.Empty, payment.PaymentType == PaymentType.Debit.ToString() ? double.Parse(payment.DebitAmount.ToString()) : double.Parse(payment.CreditAmount.ToString()));
                        }

                        if (closeResult < 0)
                        {
                            result.isSuccess = false;
                            result.message = payment.PaymentType.ToString() + "  " + payment.PaymentDate + " - " + _common.GetVISMAError(closeResult, VismaMethods.DirectPaymentClose);
                            break;
                        }
                        else if (closeResult > 0)
                        {
                            if (!isPreviousClose)
                            {
                                _awisClient.UpdateDirectPaymentsInAwis(DateTime.ParseExact(payment.PaymentDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None), closeResult.ToString(), payment.CountryCode);
                            }
                            else
                            {
                                string date = previousRowDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                                _awisClient.UpdateDirectPaymentsInAwis(DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None), closeResult.ToString(), previousCountryCode);

                                isPreviousClose = false;
                                previousRowDate = null;
                            }
                        }

                        rowCounter++;
                    }
                    this.CloseDatabase();
                    return result;
                }
                else
                {
                    this.CloseDatabase();
                    result.isSuccess = false;
                    result.message = _common.GetVISMAError(dbOpenResult, VismaMethods.DatabaseOpen);
                    return result;
                }
            }
            catch (Exception ex)
            {
                this.CloseDatabase();
                _awisClient.AddErrorToAWIS(user, ex);
                common.AddErrorLog(ex);
                result.isSuccess = false;
                result.message = Constant.exceptionError;
                return result;
            }
        }

        #endregion Direct Payments
    }
}
