﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Configuration;
using System.Web;
using System.Web.Caching;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;

namespace AWIStoVismaIntegration.Business
{
    public class AWISClient
    {
        private static string baseAddress = string.Empty;
        HttpClient _httpClient;
        Common common;
        bool localTesting = false;

        public AWISClient()
        {
            baseAddress = ConfigurationManager.AppSettings["awisAPIUrl"].ToString();
            localTesting = Convert.ToBoolean(Convert.ToInt32(ConfigurationManager.AppSettings["LocalTesting"].ToString()));
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri(baseAddress);
            common = new Common();
        }

        public bool Login(string userName, string password)
        {
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, "/Token");

                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("grant_type", "password"));
                keyValues.Add(new KeyValuePair<string, string>("username", userName));
                keyValues.Add(new KeyValuePair<string, string>("password", password));
                keyValues.Add(new KeyValuePair<string, string>("client_id", "AutoConcept"));

                request.Content = new FormUrlEncodedContent(keyValues);

                if(localTesting)
                {
                    request.RequestUri = new Uri("http://autoconcept.southeastasia.cloudapp.azure.com:9595/api/Token");
                    //request.RequestUri = new Uri("http://localhost/AutoConcept.API/Token");
                }

                var response = _httpClient.SendAsync(request);
                response.Wait();
                var contentStream = response.Result.Content.ReadAsStreamAsync();
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(LoginResult));
                LoginResult res = (LoginResult)serializer.ReadObject(contentStream.Result);
                if (HttpContext.Current != null)
                    HttpContext.Current.Cache.Remove("access_token");
                if (res.access_token != null)
                {
                    HttpRuntime.Cache.Add(
                        "access_token",
                        res.access_token,
                        null,
                        DateTime.Now.AddDays(7),
                        Cache.NoSlidingExpiration,
                        CacheItemPriority.Default,
                        null);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                common.AddErrorLog(ex);
                return false;
            }
        }

        public List<CountryViewModel> GetCountryList()
        {
            try
            {
                List<CountryViewModel> res = new List<CountryViewModel>();
                if (this.TestConnection())
                {
                    SetAuthorization();
                    var request = new HttpRequestMessage(HttpMethod.Get, baseAddress + "/api/AWIStoVisma/GetCountries");
                    var response = _httpClient.SendAsync(request);
                    response.Wait();
                    var contentStream = response.Result.Content.ReadAsStreamAsync();
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<CountryViewModel>));
                    if (contentStream.Result != null)
                        res = (List<CountryViewModel>)serializer.ReadObject(contentStream.Result);
                }
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GetCountryDBName(int countryId)
        {
            try
            {
                if (countryId > 0)
                {
                    SetAuthorization();
                    var request = new HttpRequestMessage(HttpMethod.Get, baseAddress + "/api/AWIStoVisma/GetCountryDB?countryId=" + countryId.ToString());
                    var response = _httpClient.SendAsync(request);
                    response.Wait();
                    var contentStream = response.Result.Content.ReadAsStreamAsync();
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(string));
                    string countryDB = (string)serializer.ReadObject(contentStream.Result);
                    Common.countryDBName = countryDB;
                }
                else
                {
                    Common.countryDBName = Constant.testDB;
                }
            }
            catch (SerializationException)
            {
                Common.countryDBName = Constant.testDB;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public InvoicesForVismaViewModel GetInvoicesList(string fromInvoiceNo, string toInvoiceNo, int countryId, DateTime toInvoiceDate)
        {
            try
            {
                InvoicesForVismaViewModel invoicesList = new InvoicesForVismaViewModel();
                SetAuthorization();
                var request = new HttpRequestMessage(HttpMethod.Get, baseAddress + "/api/AWIStoVisma/GetInvoices?fromInvoiceNo=" + fromInvoiceNo + "&toInvoiceNo=" + toInvoiceNo + "&countryId=" + countryId + "&toInvoiceDate=" + toInvoiceDate.ToString("yyyyMMdd"));
                var response = _httpClient.SendAsync(request);
                response.Wait();
                var contentStream = response.Result.Content.ReadAsStreamAsync();
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(InvoicesForVismaViewModel));
                invoicesList = (InvoicesForVismaViewModel)serializer.ReadObject(contentStream.Result);
                return invoicesList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CustomerViewModel GetCustomerDetail(int customerId)
        {
            try
            {
                SetAuthorization();
                var request = new HttpRequestMessage(HttpMethod.Get, baseAddress + "/api/AWIStoVisma/GetCustomerDetail?customerId=" + customerId);
                var response = _httpClient.SendAsync(request);
                response.Wait();
                var contentStream = response.Result.Content.ReadAsStreamAsync();
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(CustomerViewModel));
                CustomerViewModel customer = (CustomerViewModel)serializer.ReadObject(contentStream.Result);
                return customer;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }        

        #region Common
        public bool TestConnection()
        {
            try
            {
                //bool isOnline = false;
                SetAuthorization();
                var request = new HttpRequestMessage(HttpMethod.Get, baseAddress + "/api/AWIStoVisma/TestConnection");
                var response = _httpClient.SendAsync(request);
                response.Wait();
                if (response.Result.StatusCode.ToString() == "OK")
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                common.AddErrorLog(ex);
                return false;
            }
        }

        public void SetAuthorization()
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", " " + (string)HttpRuntime.Cache["access_token"]);
        }

        public void AddErrorToAWIS(string loggedInUser, Exception exception)
        {
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, baseAddress + "/api/AWIStoVisma/AddErrorLog");

                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("loggedinUser", loggedInUser));
                keyValues.Add(new KeyValuePair<string, string>("exception", exception.Message));
                keyValues.Add(new KeyValuePair<string, string>("innerException", exception.InnerException == null ? string.Empty : exception.InnerException.Message));
                keyValues.Add(new KeyValuePair<string, string>("application", "AwisToVISMA"));

                request.Content = new FormUrlEncodedContent(keyValues);
                var response = _httpClient.SendAsync(request);
                response.Wait();
                var contentStream = response.Result.Content.ReadAsStreamAsync();
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(int));
                serializer.ReadObject(contentStream.Result);
            }
            catch (Exception ex)
            {
                common.AddErrorLog(ex);
            }
        }
        #endregion

        #region Direct Payments

        public DirectPaymentsForVismaViewModel GetDirectPaymentsList(DateTime from, DateTime to, int countryId)
        {
            try
            {
                DirectPaymentsForVismaViewModel paymentList = new DirectPaymentsForVismaViewModel();

                SetAuthorization();

                var request = new HttpRequestMessage(HttpMethod.Get, baseAddress + "/api/AWIStoVisma/GetDirectPayments?fromDate=" + from.ToString("yyyyMMdd") + "&toDate=" + to.ToString("yyyyMMdd") + "&countryId=" + countryId);

                var response = _httpClient.SendAsync(request);
                response.Wait();
                var contentStream = response.Result.Content.ReadAsStreamAsync();
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(DirectPaymentsForVismaViewModel));

                paymentList = (DirectPaymentsForVismaViewModel)serializer.ReadObject(contentStream.Result);
                return paymentList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public DirectPaymentsForVismaViewModel GetDirectPaymentsList(DateTime from, DateTime to, int countryId)
        //{
        //    try
        //    {
        //        DirectPaymentsForVismaViewModel paymentList = new DirectPaymentsForVismaViewModel();
        //        paymentList.DirectPayments = new List<DirectPaymentsViewModel>();

        //        DirectPaymentsViewModel vm1 = new DirectPaymentsViewModel();
        //        vm1.AccountNumber = "1940";
        //        vm1.Amount = (decimal)160;
        //        vm1.DebitAmount = (decimal)160;
        //        vm1.CreditAmount = -(decimal)0.00;
        //        vm1.ShowAmount = "160";
        //        vm1.ShowDebitAmount = "160";
        //        vm1.ShowCreditAmount = string.Empty;
        //        vm1.Country = "Sweden";
        //        vm1.CreditDebit = (int)PaymentType.Debit;
        //        vm1.CustomerId = 100;
        //        vm1.CustomerName = "Test Customer Name 100";
        //        vm1.GrossValue = (decimal)10.25;
        //        vm1.NETValue = (decimal)11.75;
        //        vm1.PaymentDate = "05/01/2019";
        //        vm1.PaymentTerm = "15";
        //        vm1.PaymentType = PaymentType.Debit.ToString();
        //        vm1.Period = "1905";
        //        vm1.TaxValue = (decimal)14.75;
        //        vm1.VoucherText = "Total";
        //        paymentList.DirectPayments.Add(vm1);

        //        DirectPaymentsViewModel vm2 = new DirectPaymentsViewModel();
        //        vm2.AccountNumber = "3011";
        //        vm2.Amount = (decimal)100;
        //        vm2.DebitAmount = (decimal)0;
        //        vm2.CreditAmount = -(decimal)100;
        //        vm2.ShowAmount = "100";
        //        vm2.ShowDebitAmount = "";
        //        vm2.ShowCreditAmount = "100";
        //        vm2.Country = "Sweden";
        //        vm2.CreditDebit = (int)PaymentType.Credit;
        //        vm2.CustomerId = 101;
        //        vm2.CustomerName = "Test Customer Name 101";
        //        vm2.GrossValue = (decimal)10.25;
        //        vm2.NETValue = (decimal)11.75;
        //        vm2.PaymentDate = "05/01/2019";
        //        vm2.PaymentTerm = "15";
        //        vm2.PaymentType = PaymentType.Credit.ToString();
        //        vm2.Period = "1905";
        //        vm2.TaxValue = (decimal)14.75;
        //        vm2.VoucherText = "Total";
        //        paymentList.DirectPayments.Add(vm2);

        //        DirectPaymentsViewModel vm3 = new DirectPaymentsViewModel();
        //        vm3.AccountNumber = "3012";
        //        vm3.Amount = (decimal)60;
        //        vm3.DebitAmount = (decimal)0;
        //        vm3.CreditAmount = -(decimal)60;
        //        vm3.ShowAmount = "60";
        //        vm3.ShowDebitAmount = "";
        //        vm3.ShowCreditAmount = "60";
        //        vm3.Country = "Sweden";
        //        vm3.CreditDebit = (int)PaymentType.Credit;
        //        vm3.CustomerId = 102;
        //        vm3.CustomerName = "Test Customer Name 102";
        //        vm3.GrossValue = (decimal)10.25;
        //        vm3.NETValue = (decimal)11.75;
        //        vm3.PaymentDate = "05/01/2019";
        //        vm3.PaymentTerm = "15";
        //        vm3.PaymentType = PaymentType.Credit.ToString();
        //        vm3.Period = "1905";
        //        vm3.TaxValue = (decimal)14.75;
        //        vm3.VoucherText = "Total";
        //        paymentList.DirectPayments.Add(vm3);

        //        paymentList.TotalGrossValue = (decimal)102.50;
        //        paymentList.TotalNetValue = (decimal)95.78;
        //        paymentList.TotalTaxValue = (decimal)1025.07;

        //        return paymentList;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public void UpdateDirectPaymentsInAwis(DateTime paymentDate, string voucherNo, string countryCode)
        {
            try
            {
                SetAuthorization();
                var request = new HttpRequestMessage(HttpMethod.Get, baseAddress + "api/AWIStoVisma/UpdateDirectPaymentsInAwis?paymentDate=" + paymentDate.ToString("yyyyMMdd") + "&voucherNo=" + voucherNo + "&countryCode=" + countryCode);
                var response = _httpClient.SendAsync(request);
                response.Wait();
                var contentStream = response.Result.Content.ReadAsStreamAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public LastUpdatedData GetLastUpdatedData(string countryId)
        {
            try
            {
                SetAuthorization();
                var request = new HttpRequestMessage(HttpMethod.Get, baseAddress + "/api/AWIStoVisma/GetLastUpdatedData?countryId=" + countryId);
                var response = _httpClient.SendAsync(request);
                response.Wait();
                var contentStream = response.Result.Content.ReadAsStreamAsync();
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(LastUpdatedData));
                LastUpdatedData lastUpdatedData = (LastUpdatedData)serializer.ReadObject(contentStream.Result);
                return lastUpdatedData;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion
    }
}
