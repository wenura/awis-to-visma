﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWIStoVismaIntegration.Business
{
    public class Common
    {
        public static string countryDBName = string.Empty;


        public void AddErrorLog(Exception ex)
        {
            File.AppendAllText(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\LogFile.txt", string.Format(Environment.NewLine + DateTime.Now + " Error Occured" + ex.Message.ToString(), Environment.NewLine + (ex.InnerException != null ? ex.InnerException.Message : string.Empty), Environment.NewLine));
        }

        public void AddVISMALog(string text)
        {
            File.AppendAllText(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\LogFile.txt", string.Format(Environment.NewLine + DateTime.Now + " - " + text));
        }

        #region ErrorDictionary
        public void FeedVISMAErrorDictionary()
        {
            #region DatabaseOpen
            Dictionary<int, string> dict1 = new Dictionary<int, string>();
            dict1.Add(-1, "The path to the database is wrong.");
            dict1.Add(-2, "Database must be released.");
            dict1.Add(-3, "Wrong version, the database must be updated.");
            dict1.Add(-4, "Incorrect version, the API must be updated.");
            dict1.Add(-5, "Database is already open.");
            dict1.Add(-6, "Can not log in to the database.");
            Constant.vismaMethods.Add(VismaMethods.DatabaseOpen.ToString(), dict1);
            #endregion

            #region PersonFind
            Dictionary<int, string> dict2 = new Dictionary<int, string>();
            dict2.Add(-1, "No record of person found.");
            Constant.vismaMethods.Add(VismaMethods.PersonFind.ToString(), dict2);
            #endregion

            #region PersonAdd
            Dictionary<int, string> dict3 = new Dictionary<int, string>();
            dict3.Add(-1, "Person already exists.");
            dict3.Add(-2, "Person ID is too long.");
            Constant.vismaMethods.Add(VismaMethods.PersonAdd.ToString(), dict3);
            #endregion

            #region InvoiceOpen
            Dictionary<int, string> dict4 = new Dictionary<int, string>();
            dict4.Add(-1, "The database was not open.");
            dict4.Add(-2, "No financial year for this date.");
            dict4.Add(-3, "Financial year is closed or completed for this date.");
            dict4.Add(-4, "The person could not be found.");
            Constant.vismaMethods.Add(VismaMethods.InvoiceOpen.ToString(), dict4);
            #endregion

            #region InvoiceAddRow
            Dictionary<int, string> dict5 = new Dictionary<int, string>();
            dict5.Add(-1, "The database is not open.");
            dict5.Add(-2, "InvoiceOpen has not been called.");
            dict5.Add(-3, "Account is missing in the database.");
            dict5.Add(-4, "Object1 is missing or has the wrong type.");
            dict5.Add(-5, "Object2 is missing or has the wrong type.");
            dict5.Add(-6, "The specified item can not be found.");
            Constant.vismaMethods.Add(VismaMethods.InvoiceAddRow.ToString(), dict5);
            #endregion

            #region InvoiceClose
            Dictionary<int, string> dict6 = new Dictionary<int, string>();
            dict6.Add(-1, "The database is not open.");
            dict6.Add(-2, "InvoiceOpen has not been called.");
            dict6.Add(-3, "Invoice could not be saved.");
            Constant.vismaMethods.Add(VismaMethods.InvoiceClose.ToString(), dict5);
            #endregion
        }
        #endregion

        public string GetVISMAError(int errorId, VismaMethods methodName)
        {
            string errorMsg = string.Empty;
            var res = Constant.vismaMethods.FirstOrDefault(o => o.Key == methodName.ToString()).Value;
            errorMsg = res.FirstOrDefault(o => o.Key == errorId).Value;

            return errorMsg != string.Empty ? errorMsg : Constant.vismaError;
        }


    }
}
